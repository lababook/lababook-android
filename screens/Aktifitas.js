import React from 'react'
import { View, Text } from 'react-native'

import { textExtraProps as tProps } from '../config/system'

export default function Aktifitas () {
  return (
    <View>
      <Text {...tProps}>Aktifitas</Text>
    </View>
  )
}
